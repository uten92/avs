from django.urls import path, include

import rs.urls


urlpatterns = [
    path('rs/', include(rs.urls.urlpatterns)),
]