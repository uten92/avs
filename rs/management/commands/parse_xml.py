import datetime
from pathlib import Path
import xml.etree.ElementTree as ET

from django.core.management.base import BaseCommand
from rs import models as rs_models


class Command(BaseCommand):
    help = 'Парсинг xml'
    carriers = {}
    airports = {}

    def add_arguments(self, parser):
        parser.add_argument('file_xml', type=Path)

    def _parse_itinerary(self, flights_info):
        itineraries = []
        for itinerary in flights_info.iter('Flight'):
            itinerary_dict = {}
            for itinerary_info in itinerary:

                if itinerary_info.tag == 'Carrier':
                    short_name, full_name = itinerary_info.attrib['id'], itinerary_info.text
                    carrier = self.carriers.get(short_name)
                    if not carrier:
                        carrier = rs_models.Carrier.objects.create(short_name=short_name, full_name=full_name)
                        self.carriers[itinerary_info.attrib['id']] = carrier
                    itinerary_dict['carrier'] = carrier

                elif itinerary_info.tag == 'TicketType':
                    itinerary_dict['ticket_type'] = itinerary_info.text

                elif itinerary_info.tag == 'FlightNumber':
                    itinerary_dict['flight_number'] = itinerary_info.text

                elif itinerary_info.tag in ('Source', 'Destination'):
                    airport = self.airports.get(itinerary_info.text)
                    if not airport:
                        airport = rs_models.Airport.objects.create(short_name=itinerary_info.text)
                        self.airports[itinerary_info.text] = airport
                    itinerary_dict[itinerary_info.tag.lower()] = airport

                elif itinerary_info.tag == 'ArrivalTimeStamp':
                    itinerary_dict['arrival'] = datetime.datetime.strptime(itinerary_info.text, '%Y-%m-%dT%H%M')
                elif itinerary_info.tag == 'DepartureTimeStamp':
                    itinerary_dict['departure'] = datetime.datetime.strptime(itinerary_info.text, '%Y-%m-%dT%H%M')

                elif itinerary_info.tag == 'Class':
                    itinerary_dict['flight_class'] = itinerary_info.text

                elif itinerary_info.tag == 'NumberOfStops':
                    itinerary_dict['number_of_stops'] = itinerary_info.text

                elif itinerary_info.tag == 'FareBasis':
                    itinerary_dict['fare_basis'] = itinerary_info.text

            itineraries.append(itinerary_dict)

        return itineraries

    def handle(self, *args, **options):
        self.carriers = {c.short_name: c for c in rs_models.Carrier.objects.all().only('id', 'short_name')}
        self.airports = {a.short_name: a for a in rs_models.Airport.objects.all().only('id', 'short_name')}
        price_types = {
            'SingleAdult': rs_models.Pricing.SINGLE_ADULT,
            'SingleChild': rs_models.Pricing.SINGLE_CHILD,
            'SingleInfant': rs_models.Pricing.SINGLE_INFANT,
        }

        path_file = options['file_xml']

        tree = ET.parse(path_file)
        root = tree.getroot()
        for head in root:
            for flights in head:
                itinerary_bulk_create = []
                pricing = {}
                flight_obj = rs_models.Flight()
                for flights_info in flights:
                    if flights_info.tag == 'Pricing':
                        for price in flights_info:
                            type_price, charge_type = price.attrib['type'], price.attrib['ChargeType']
                            if type_price not in pricing:
                                pricing[type_price] = {}
                            pricing[type_price][charge_type] = price.text
                    else:
                        itineraries = self._parse_itinerary(flights_info)
                        if flights_info.tag == 'OnwardPricedItinerary':
                            type_direction = rs_models.Itinerary.ONWARD_FLIGHT
                            flight_obj.source = itineraries[0]['source']
                            flight_obj.destination = itineraries[-1]['destination']
                            flight_obj.departure = itineraries[0]['departure']
                            flight_obj.arrival = itineraries[-1]['arrival']
                        else:
                            type_direction = rs_models.Itinerary.RETURN_FLIGHT
                        for i_dict in itineraries:
                            itinerary_bulk_create.append(
                                rs_models.Itinerary(type_direction=type_direction, **i_dict)
                            )

                flight_obj.save()
                for itinerary_obj in itinerary_bulk_create:
                    itinerary_obj.flight = flight_obj
                rs_models.Itinerary.objects.bulk_create(itinerary_bulk_create)

                pricing_bulk_create = []
                for type_price, price_info in pricing.items():
                    pricing_bulk_create.append(
                        rs_models.Pricing(
                            flight=flight_obj,
                            type_charges=price_types[type_price],
                            base_fare=price_info.get('BaseFare'),
                            airline_taxes=price_info.get('AirlineTaxes'),
                            total_amount=price_info.get('TotalAmount'),
                        )
                    )
                rs_models.Pricing.objects.bulk_create(pricing_bulk_create)



