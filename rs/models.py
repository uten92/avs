from django.db import models
from django.db.models import Prefetch,F, ExpressionWrapper, DurationField


class Carrier(models.Model):
    short_name = models.CharField(max_length=2, unique=True)
    full_name = models.CharField(max_length=128, unique=True)


class Airport(models.Model):
    short_name = models.CharField(max_length=3, unique=True)


class FlightQuerySet(models.QuerySet):
    def with_detail_info(self):
        return self.select_related().prefetch_related(
            Prefetch('itinerary', queryset=Itinerary.objects.select_related()),
            Prefetch('prices', queryset=Pricing.objects.all()),
        )

    def with_time_flight(self):
        wrapped_expression = ExpressionWrapper(F('arrival') - F('departure'), output_field=DurationField())
        return self.annotate(time_flight=wrapped_expression)


class Flight(models.Model):
    source = models.ForeignKey(Airport, on_delete=models.CASCADE, related_name='source_flights')
    destination = models.ForeignKey(Airport, on_delete=models.CASCADE, related_name='destination_flights')
    departure = models.DateTimeField()
    arrival = models.DateTimeField()
    objects = FlightQuerySet.as_manager()


class Pricing(models.Model):
    SINGLE_ADULT = 1
    SINGLE_CHILD = 2
    SINGLE_INFANT = 3
    TYPE_CHOICES = (
        (SINGLE_ADULT, 'single_adult'),
        (SINGLE_CHILD, 'single_child'),
        (SINGLE_INFANT, 'single_infant'),
    )

    base_fare = models.FloatField(null=True)
    airline_taxes = models.FloatField(null=True)
    total_amount = models.FloatField(null=True)
    type_charges = models.PositiveIntegerField(choices=TYPE_CHOICES, default=SINGLE_ADULT)
    flight = models.ForeignKey(Flight, on_delete=models.CASCADE, related_name='prices')


class Itinerary(models.Model):
    ONWARD_FLIGHT = 1
    RETURN_FLIGHT = 2
    TYPE_CHOICES = (
        (ONWARD_FLIGHT, 'onward_flight'),
        (RETURN_FLIGHT, 'return_flight'),
    )

    carrier = models.ForeignKey(Carrier, on_delete=models.CASCADE, related_name='flights')
    source = models.ForeignKey(Airport, on_delete=models.CASCADE, related_name='source_itinerary')
    destination = models.ForeignKey(Airport, on_delete=models.CASCADE, related_name='destination_itinerary')
    flight = models.ForeignKey(Flight, on_delete=models.CASCADE, related_name='itinerary')
    type_direction = models.PositiveIntegerField(choices=TYPE_CHOICES)
    flight_number = models.PositiveSmallIntegerField()
    departure = models.DateTimeField()
    arrival = models.DateTimeField()
    flight_class = models.CharField(max_length=1)
    number_of_stops = models.PositiveSmallIntegerField()
    fare_basis = models.TextField()
    ticket_type = models.CharField(max_length=1)
