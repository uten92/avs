from rest_framework.generics import ListAPIView
from rest_framework import response
from django.db.models import Count, F

from rs.models import Pricing, Flight
from rs import serializers
from rs import filters
from rs import validators


class FlightView(ListAPIView):
    queryset = Flight.objects.with_detail_info()
    filter_class = filters.FlightFilter
    serializer_class = serializers.FlightSerializer


class FlightAnalysisView(FlightView):
    filter_class = filters.FlightAnalysisFilter

    def get(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        result = {}

        qs_price = queryset.filter(
            prices__type_charges=Pricing.SINGLE_ADULT
        ).annotate(count_itinerary=Count('itinerary'))
        if qs_price.exists():
            qs_price = qs_price.order_by('prices__total_amount')
            obj_min_price = qs_price.first()
            obj_max_price = qs_price.last()
            result['min_price'] = self.serializer_class(obj_min_price).data
            result['max_price'] = self.serializer_class(obj_max_price).data

            obj_optimal = qs_price.order_by('count_itinerary', 'prices__total_amount').first()
            result['optimal'] = self.serializer_class(obj_optimal).data

        qs_order_by_time_flight = queryset.with_time_flight().order_by('time_flight')
        if qs_order_by_time_flight.exists():
            obj_min_time_flight = qs_order_by_time_flight.first()
            obj_max_time_flight = qs_order_by_time_flight.last()
            result['min_time_flight'] = self.serializer_class(obj_min_time_flight).data
            result['max_time_flight'] = self.serializer_class(obj_max_time_flight).data

        return response.Response(result, status=200)


class FlightCompareView(ListAPIView):

    def get(self, request, *args, **kwargs):
        validator = validators.FlightsValidator(data=self.request.GET)
        validator.is_valid(raise_exception=True)
        flight_ids = validator.validated_data['flights']

        value_fields = {
            'count_itinerary': 'count_itinerary',
            'time_flight': 'time_flight',
            'source__short_name': 'source_airport',
            'destination__short_name': 'destination_airport',
            'price_single_adult': 'price_single_adult'
        }
        fields = value_fields.keys()

        queryset = Flight.objects.with_detail_info().with_time_flight().filter(
            id__in=flight_ids,
            prices__type_charges=Pricing.SINGLE_ADULT,
        ).annotate(
            count_itinerary=Count('itinerary'),
            price_single_adult=F('prices__total_amount'),
        ).values('id', *fields)

        flight_1, flight_2 = queryset
        flight_1_info = {'id': flight_1['id']}
        flight_2_info = {'id': flight_2['id']}

        for k in fields:
            if flight_1[k] != flight_2[k]:
                field = value_fields[k]
                flight_1_info[field] = flight_1[k]
                flight_2_info[field] = flight_2[k]

        return response.Response([flight_1_info, flight_2_info])
