from django.conf.urls import url
from rs import views


urlpatterns = [
    url(r'^flights/$', views.FlightView.as_view(), name='flights'),
    url(r'^flights/analysis/$', views.FlightAnalysisView.as_view(), name='flight_analysis'),
    url(r'^flights/compare/$', views.FlightCompareView.as_view(), name='flight_compare'),
]
