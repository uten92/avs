from django_filters import filterset

from rs.models import Flight


class FlightFilter(filterset.FilterSet):
    source = filterset.CharFilter(field_name='source__short_name')
    destination = filterset.CharFilter(field_name='destination__short_name')

    class Meta:
        model = Flight
        fields = ('source', 'destination', )


class FlightAnalysisFilter(filterset.FilterSet):
    source = filterset.CharFilter(field_name='source__short_name', required=True)
    destination = filterset.CharFilter(field_name='destination__short_name', required=True)

    class Meta:
        model = Flight
        fields = ('source', 'destination', )
