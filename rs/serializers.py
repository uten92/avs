from rest_framework import serializers

from rs import models


class CarrierSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Carrier
        fields = ('short_name', 'full_name', )


class AirportSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Airport
        fields = ('short_name', )


class PricingSerializer(serializers.ModelSerializer):
    type_charges = serializers.SerializerMethodField()

    class Meta:
        model = models.Pricing
        fields = ('base_fare', 'airline_taxes', 'total_amount', 'type_charges')

    @staticmethod
    def get_type_charges(obj):
        return obj.get_type_charges_display()


class ItinerarySerialaizer(serializers.ModelSerializer):
    carrier = CarrierSerializer()
    source = AirportSerializer()
    destination = AirportSerializer()
    type_direction = serializers.SerializerMethodField()
    departure = serializers.DateTimeField(format='%d.%m.%Y %H:%M')
    arrival = serializers.DateTimeField(format='%d.%m.%Y %H:%M')

    class Meta:
        model = models.Itinerary
        fields = (
            'carrier', 'source', 'destination', 'flight', 'flight_number',
            'departure', 'arrival', 'flight_class', 'number_of_stops', 'fare_basis', 'ticket_type', 'type_direction',
        )

    @staticmethod
    def get_type_direction(obj):
        return obj.get_type_direction_display()


class FlightSerializer(serializers.ModelSerializer):
    source = AirportSerializer()
    destination = AirportSerializer()
    prices = PricingSerializer(many=True)
    itinerary = ItinerarySerialaizer(many=True)
    departure = serializers.DateTimeField(format='%d.%m.%Y %H:%M')
    arrival = serializers.DateTimeField(format='%d.%m.%Y %H:%M')

    class Meta:
        model = models.Flight
        fields = ('id', 'source', 'destination', 'departure', 'arrival', 'itinerary', 'prices')
