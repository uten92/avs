import datetime

import factory
from factory.django import DjangoModelFactory

from rs import models as rs_models


class CarrierFactory(DjangoModelFactory):
    short_name = factory.Sequence(lambda n: f'A{int(str(n)[-1])}')
    full_name = factory.Sequence(lambda n: f'full name {int(str(n)[-1])}')

    class Meta:
        model = rs_models.Carrier


class AirportFactory(DjangoModelFactory):
    short_name = factory.Sequence(lambda n: f'AB{int(str(n)[-1])}')

    class Meta:
        model = rs_models.Airport


class FlightFactory(DjangoModelFactory):
    source = factory.SubFactory(AirportFactory)
    destination = factory.SubFactory(AirportFactory)
    departure = datetime.datetime.now()
    arrival = datetime.datetime.now() + datetime.timedelta(hours=5)

    class Meta:
        model = rs_models.Flight


class PricingFactory(DjangoModelFactory):
    base_fare = 100
    airline_taxes = 50
    total_amount = 150
    type_charges = rs_models.Pricing.SINGLE_ADULT
    flight = factory.SubFactory(FlightFactory)

    class Meta:
        model = rs_models.Pricing


class ItineraryFactory(DjangoModelFactory):
    carrier = factory.SubFactory(CarrierFactory)
    source = factory.SubFactory(AirportFactory)
    destination = factory.SubFactory(AirportFactory)
    flight = factory.SubFactory(FlightFactory)
    type_direction = rs_models.Itinerary.ONWARD_FLIGHT
    flight_number = 100
    departure = datetime.datetime.now()
    arrival = datetime.datetime.now() + datetime.timedelta(hours=5)
    flight_class = 'E'
    number_of_stops = 0
    fare_basis = factory.Sequence(lambda n: f'fare basis {n}')
    ticket_type = 'A'

    class Meta:
        model = rs_models.Itinerary
