import datetime

from parameterized import parameterized
from django.test import TestCase
from django.urls import reverse
from django.db.models import Count, F

from rs.tests import factories
from rs.models import Pricing, Flight


class FlightViewTestCase(TestCase):
    uri = reverse('flights')

    def test_params(self):
        factories.ItineraryFactory.create()
        itinerary = factories.ItineraryFactory.create()
        flight = itinerary.flight
        pricing = factories.PricingFactory.create(flight=flight)
        carrier = itinerary.carrier

        response = self.client.get(self.uri, data={'source': 'ABC'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), [])

        response = self.client.get(self.uri, data={'source': flight.source.short_name})
        result = [
            {
                'id': flight.id,
                'source': {'short_name': flight.source.short_name},
                'destination': {'short_name': flight.destination.short_name},
                'departure': flight.departure.strftime('%d.%m.%Y %H:%M'),
                'arrival': flight.arrival.strftime('%d.%m.%Y %H:%M'),
                'itinerary': [
                    {
                        'carrier': {'short_name': carrier.short_name, 'full_name': carrier.full_name},
                        'source': {'short_name': itinerary.source.short_name},
                        'destination': {'short_name': itinerary.destination.short_name},
                        'flight': flight.id,
                        'flight_number': itinerary.flight_number,
                        'departure': itinerary.departure.strftime('%d.%m.%Y %H:%M'),
                        'arrival': itinerary.arrival.strftime('%d.%m.%Y %H:%M'),
                        'flight_class': itinerary.flight_class,
                        'number_of_stops': itinerary.number_of_stops,
                        'fare_basis': itinerary.fare_basis,
                        'ticket_type': itinerary.ticket_type,
                        'type_direction': itinerary.get_type_direction_display()
                    }
                ],
                'prices': [
                    {
                        'base_fare': pricing.base_fare, 'airline_taxes': pricing.airline_taxes,
                        'total_amount': pricing.total_amount, 'type_charges': pricing.get_type_charges_display()
                    }
                ]
            }
        ]
        self.assertListEqual(response.json(), result)

        response = self.client.get(self.uri, data={'destination': flight.destination.short_name})
        self.assertEqual(response.json(), result)

        response = self.client.get(self.uri, data={'source': flight.source.short_name, 'destination': 'ABC'})
        self.assertEqual(response.json(), [])

    @parameterized.expand([('post', ), ('put', ), ('delete', )])
    def test_method(self, method):
        request = getattr(self.client, method)
        response = request(self.uri)
        self.assertEqual(response.status_code, 405)

    def test_num_queries(self):
        for _ in range(5):
            flight = factories.FlightFactory.create()
            factories.ItineraryFactory.create(source=flight.source, destination=flight.destination, flight=flight)
            factories.PricingFactory.create(flight=flight)

        with self.assertNumQueries(3):
            response = self.client.get(self.uri)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 5)


class FlightAnalysisViewTestCase(TestCase):
    uri = reverse('flight_analysis')

    @parameterized.expand([('post', ), ('put', ), ('delete', )])
    def test_method(self, method):
        request = getattr(self.client, method)
        response = request(self.uri)
        self.assertEqual(response.status_code, 405)

    def test_params(self):
        response = self.client.get(self.uri, data={'source': 'ABC'})
        self.assertEqual(response.status_code, 400)

        response = self.client.get(self.uri, data={'destination': 'ABC'})
        self.assertEqual(response.status_code, 400)

        response = self.client.get(self.uri, data={'source': 'ABC', 'destination': 'ABC'})
        self.assertNotEqual(response.status_code, 400)

    @parameterized.expand(
        [
            ([20, 30, 50, 60], [2, 5, 3, 4], ),
            ([80, 30, 10, 40], [6, 3, 2, 9], ),
            ([100, 10, 80, 40], [3, 5, 7, 4], ),
        ]
    )
    def test_options_result(self, prices, flight_hours):
        source_airport = factories.AirportFactory.create(short_name='PUL')
        destination_airport = factories.AirportFactory.create(short_name='KOL')
        for element in range(len(prices)):
            departure = datetime.datetime.now()
            arrival = departure + datetime.timedelta(hours=flight_hours[element])
            flight = factories.FlightFactory.create(
                arrival=arrival, departure=departure, source=source_airport, destination=destination_airport
            )
            for i in range(element + 1):
                factories.ItineraryFactory.create(
                    source=flight.source, destination=flight.destination, flight=flight,
                    arrival=arrival, departure=departure,
                )
            factories.PricingFactory.create(
                flight=flight, type_charges=Pricing.SINGLE_ADULT, total_amount=prices[element]
            )

        response = self.client.get(
            self.uri,
            data={'source': source_airport.short_name, 'destination': destination_airport.short_name}
        )
        self.assertEqual(response.status_code, 200)

        max_price_obj = Pricing.objects.all().order_by('total_amount').last().flight
        min_price_obj = Pricing.objects.all().order_by('total_amount').first().flight
        max_time_flight = Flight.objects.all().order_by('arrival').last()
        min_time_flight = Flight.objects.all().order_by('arrival').first()

        optimal_flight = Flight.objects.annotate(
            count_itinerary=Count('itinerary')
        ).order_by('count_itinerary', 'prices__total_amount').first()

        self.assertEqual(response.json()['min_price']['id'], min_price_obj.id)
        self.assertEqual(response.json()['max_price']['id'], max_price_obj.id)
        self.assertEqual(response.json()['min_time_flight']['id'], min_time_flight.id)
        self.assertEqual(response.json()['max_time_flight']['id'], max_time_flight.id)
        self.assertEqual(response.json()['optimal']['id'], optimal_flight.id)


class FlightCompareViewTestCase(TestCase):
    uri = reverse('flight_compare')

    @parameterized.expand([('post', ), ('put', ), ('delete', )])
    def test_method(self, method):
        request = getattr(self.client, method)
        response = request(self.uri)
        self.assertEqual(response.status_code, 405)

    def test_params(self):
        response = self.client.get(self.uri)
        self.assertEqual(response.status_code, 400)

        response = self.client.get(self.uri, data={'flight_ids': 2})
        self.assertEqual(response.status_code, 400)

        response = self.client.get(self.uri, data={'flight_ids': [1, 2]})
        self.assertEqual(response.status_code, 400)

    def test_compare(self):
        flight_1 = factories.FlightFactory.create(
            departure=datetime.datetime(2019, 8, 29, 16, 30),
            arrival=datetime.datetime(2019, 8, 29, 16, 30) + datetime.timedelta(hours=5)
        )
        factories.ItineraryFactory.create(flight=flight_1, source=flight_1.source, destination=flight_1.destination)
        factories.ItineraryFactory.create(flight=flight_1, source=flight_1.source, destination=flight_1.destination)
        price_1 = factories.PricingFactory.create(flight=flight_1, total_amount=200)

        flight_2 = factories.FlightFactory.create(
            departure=datetime.datetime(2019, 8, 29, 13, 30),
            arrival=datetime.datetime(2019, 8, 29, 13, 30) + datetime.timedelta(hours=2)
        )
        factories.ItineraryFactory.create(flight=flight_2, source=flight_2.source, destination=flight_2.destination)
        price_2 = factories.PricingFactory.create(flight=flight_2,  total_amount=100)

        response = self.client.get(self.uri, data={'flights': [flight_1.id, flight_2.id]})
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(
            response.json(),
            [
                {
                    'id': flight_1.id,
                    'count_itinerary': 2,
                    'time_flight': '18000.0',
                    'source_airport': flight_1.source.short_name,
                    'destination_airport': flight_1.destination.short_name,
                    'price_single_adult': price_1.total_amount
                },
                {
                    'id': flight_2.id,
                    'count_itinerary': 1,
                    'time_flight': '7200.0',
                    'source_airport': flight_2.source.short_name,
                    'destination_airport': flight_2.destination.short_name,
                    'price_single_adult': price_2.total_amount
                },
            ]
        )
