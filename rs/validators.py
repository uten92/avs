from rest_framework import serializers

from rs import models as rs_models


class FlightsValidator(serializers.Serializer):
    flights = serializers.ListField(
        required=True, max_length=2, min_length=2,
        child=serializers.PrimaryKeyRelatedField(queryset=rs_models.Flight.objects.all().values_list('id', flat=True))
    )

