# Generated by Django 2.2.4 on 2019-08-27 13:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Airport',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('short_name', models.CharField(max_length=3, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Carrier',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('short_name', models.CharField(max_length=2, unique=True)),
                ('full_name', models.CharField(max_length=128, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Flight',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('departure', models.DateTimeField()),
                ('arrival', models.DateTimeField()),
                ('destination', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='destination_flights', to='rs.Airport')),
                ('source', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='source_flights', to='rs.Airport')),
            ],
        ),
        migrations.CreateModel(
            name='Pricing',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('base_fare', models.FloatField(null=True)),
                ('airline_taxes', models.FloatField(null=True)),
                ('total_amount', models.FloatField(null=True)),
                ('type_charges', models.PositiveIntegerField(choices=[(1, 'single_adult'), (2, 'single_child'), (3, 'single_infant')], default=1)),
                ('flight', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='prices', to='rs.Flight')),
            ],
        ),
        migrations.CreateModel(
            name='Itinerary',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type_direction', models.PositiveIntegerField(choices=[(1, 'onward_flight'), (2, 'return_flight')])),
                ('flight_number', models.PositiveSmallIntegerField()),
                ('departure', models.DateTimeField()),
                ('arrival', models.DateTimeField()),
                ('flight_class', models.CharField(max_length=1)),
                ('number_of_stops', models.PositiveSmallIntegerField()),
                ('fare_basis', models.TextField()),
                ('ticket_type', models.CharField(max_length=1)),
                ('carrier', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='flights', to='rs.Carrier')),
                ('destination', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='destination_itinerary', to='rs.Airport')),
                ('flight', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='itinerary', to='rs.Flight')),
                ('source', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='source_itinerary', to='rs.Airport')),
            ],
        ),
    ]
