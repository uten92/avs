## Для запуска сервиса необходимо выполнить:
`docker-compose build`       
`docker-compose up`


Все запросы выполняются методом GET, результат возвращается в json

Для парсинга xml файлов служит команда parse_xml. Пример использования:                                                                                                                                                                       
`./manage.py parse_xml rs/xml_files/RS_Via-3.xml`                                                                                                                                                 
`./manage.py parse_xml rs/xml_files/RS_ViaOW.xml`                                                                                                            

## Примеры запросов:

Список всех перелетов:                                                                       
`rs/flights/`

Варианты перелетов из DXB в BKK:	                                      
`rs/flights/?source=DXB&destination=BKK`

Самый дорогой/дешёвый, быстрый/долгий и оптимальный вариант:                                                           
`rs/flights/analysis/?source=DXB&destination=BKK`


Отличия двух вариантов перелета:                                                                               
`rs/flights/compare/?flights=1&flights=2`   
где flights содержит id перелетов
