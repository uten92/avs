#!/bin/bash

echo "Apply database migrations"
python manage.py migrate

echo "load fixtures"
python manage.py loaddata rs/fixtures.json

echo "Starting server"
python manage.py runserver 0.0.0.0:8000
